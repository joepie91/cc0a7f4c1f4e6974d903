Promise = require "bluebird"
appUtil = require "../util"

module.exports = (shelf) ->
	shelf.model "User",
		tableName: "users"
		hasTimestamps: true

		projects: -> @hasMany "Project", "owner_id"

		enable: ->
			if @get "activated"
				throw new util.errors.RequirementsError("The user is already enabled.")
			else
				@set "activated", true
				@saveChanges()

		disable: ->
			if not @get "activated"
				throw new util.errors.RequirementsError("The user is already disabled.")
			else
				@set "activated", false
				@saveChanges()

		verifyPassword: (password) ->
			return appUtil.verifyHash @get("hash"), password

		setPassword: (password) ->
			Promise.try ->
				appUtil.hash password
			.then (hash) =>
				@set "hash", hash
				@saveChanges()
